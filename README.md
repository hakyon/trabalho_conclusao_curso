### Universidade Tuiuti do Parana ###
### Ciência da Computação ###
### Disciplina: Trabalho de conclusão de curso ###

### Exercicios/Trabalhos ###

* Periodo : 07/2021 - 12/2021
* Version 1.21
* Ambiente : Desktop

### Ferramentas usadas ###

* Terminal : Python 
* Plataforma : Linux/Windows

### Assuntos ###

* Machining Learning


### Autor  ###

* Alan Azevedo Bancks

### Contato ###

* E-mail: dsalan@hotmail.com
