import pandas as pd
import numpy as np
import time
import logging
import warnings
import warnings
warnings.filterwarnings('ignore')  # "error", "ignore", "always", "default", "module" or "once"

from sklearn import metrics
from sklearn.metrics import f1_score
import seaborn as sns
import pandas as pd
import preprocessor as preprocessor
from sklearn.metrics import mean_absolute_error, accuracy_score
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
from sklearn.pipeline import Pipeline
from tqdm import tqdm
from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.metrics import plot_confusion_matrix
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score,precision_score
from sklearn.svm import SVC, LinearSVC, NuSVC
from utils import tools



class TreinoModelos:

    def __init__(self, dataset_processado, y_full, dado_original,
                 transformada, dataset_processado_teste,
                 y_full_teste, dataset_original_teste):
        """
        description: classe responsavel por inicializar os modelos e preparar os dados para treino
        """
        self.tool = tools.Tools()

        # inicializando variaveis de uso local da classe
        self.y_full = y_full
        self.y_full_teste = y_full_teste
        self.dado_original = dado_original
        self.transformacao = transformada

        # treino e testes fixos
        self.modelo = self.svm_kernel_sigmoid()

        self.standard_scaler = preprocessing.StandardScaler()

        # padronizando dados para formato de matriz
        self.dados_padronizados = self.standard_scaler.fit_transform(dataset_processado)
        self.dados_padronizados_teste = self.standard_scaler.fit_transform(dataset_processado_teste)

        logging.info("Treinando modelo com SVM ...")
        self.treino_modelo(modelo=self.modelo, X=self.dados_padronizados, y=self.y_full)
        logging.info("Previsao finalizada")
        # self.estimativa_de_previsao(dados=dataset_processado, modelo=self.modelo)

    def svm_kernel_rbf(self):
        """
        description: metodo para retornor e ajustar kernel rbf
        """

        r_range = np.array([0.0, 1])
        gamma_range_rbf = np.array([0.1, 0.01])
        c_range_rbf = ([50, 100, 150, 300, 500, 600])
        d_range = np.array([2, 3, 4])
        param_grid_rbf = dict(gamma=gamma_range_rbf, C=c_range_rbf)

        # usando GridSearch
        # return GridSearchCV(modelo_v2, param_grid_rbf)
        # Sem usar
        return SVC(kernel='rbf', C=1000, gamma=0.1)

    def svm_kernel_linear(self):
        """
        description: metodo para retornor e ajustar kernel linear
        """
        d_range_linear = np.array([2, 3, 4, 6])
        c_range_linear = np.array([100, 110])
        param_grid_linear = dict(C=c_range_linear)
        #modelo = SVC(kernel="linear", C=c_range_linear, probability=True, gamma=0.1)
        return SVC(kernel="linear", C=80,gamma=0.1)
        #return GridSearchCV(modelo, param_grid_linear)

    def svm_kernel_poly(self):
        """
        description: metodo para retornor e ajustar kernel polinomial
        """

        c_range_poly = np.array([50., 100., 200.])
        d_range = np.array([2, 3, 4])
        r_range = np.array([0.0, 1])
        gamma_range_poly = np.array([0.3 * 0.001, 0.001, 3 * 0.001])
        param_grid_poly = dict(gamma=gamma_range_poly, degre=d_range, coef0=r_range, c=c_range_poly)
        modelo_v3 = SVC(kernel='poly')

        #return GridSearchCV(modelo_v3, param_grid_poly, cv=3)
        return SVC(kernel='poly',gamma=0.1,C=600)

    def svm_kernel_sigmoid(self):
        """
        description:  metodo para retornar e ajustar kernel sigmoid
        :return:
        """
        return SVC(kernel='sigmoid', gamma=0.1, C=1800)

    def svm_kernel_precomputed(self):
        """
        description:  metodo para retornar e ajustar kernel precomputed
        :return:
        """
        return SVC(kernel='precomputed', gamma=0.1, C=600)

    def treino_modelo(self, modelo=None, X=None, y=None):
        """
        # funcao responsal por execucao do modelo classificador preditivo
        :param modelo:
        :type modelo:
        :param X:
        :type X:
        :param y:
        :type y:
        """

        modelo = modelo

        X_valid_teste = self.dados_padronizados_teste
        y_valid_teste = self.y_full_teste

        logging.info("Efetuando divisao Treino-Teste")
        X_train, X_valid, y_train, y_valid = train_test_split(X, y, train_size=0.7, test_size=0.3, random_state=0)

        start = time.time()
        modelo.fit(X_train, y_train)
        end = time.time()
        #print(modelo.class_weight_.size)
        # Fazer predicoes com o modelo orignal da divisao treino-teste
        # prediction = modelo.predict(X_valid)

        # fazer predicao com com teste novo
        prediction = modelo.predict(X_valid_teste)

        # Avaliando modelos
        # accuracy = accuracy_score(y_valid, prediction)

        # usando teste fixo novo
        accuracy = accuracy_score(y_valid_teste, prediction)
        precision = precision_score(y_valid_teste, prediction,  average="macro")

        print("Precisao: %.2f%% \n\n" % (precision * 100.0))
        print("Accuracia: %.2f%% \n\n" % (accuracy * 100.0))
        print("Tempo de treinamento do modelo: %.2f segundos \n " % (end - start))

        # confusion_matrix(y_valid, prediction)
        confusion_matrix(y_valid_teste, prediction)

        # print("Melhores parametros escolhidos no modelo : ",modelo.best_params_)

        # print(classification_report(y_valid, prediction))
        print(classification_report(y_valid_teste, prediction))


        logging.info("[!] Plotagem da matriz de confusao")
        #print(modelo.classes_)

        # labels = ['apoio', 'ambiental', 'policial_leve' 'policial_grave']
        labels = ['ambiental', 'apoio', 'policial_leve', 'policial_grave', 'nao categorizado']

        if modelo.class_weight_.size == 1:
            labels = ['ambiental']
        if modelo.class_weight_.size == 2:
            labels = ['ambiental', 'apoio']
        if modelo.class_weight_.size == 3:
            labels = ['ambiental', 'apoio', 'policial_leve']
        if modelo.class_weight_.size == 4:
            labels = ['ambiental', 'apoio', 'policial_leve', 'policial_grave']
        if modelo.class_weight_.size == 5:
            labels = ['ambiental', 'apoio', 'policial_leve', 'policial_grave', 'nao categorizado']

        try:
            fig = plot_confusion_matrix(modelo, X_valid, y_valid, display_labels=labels)
            #fig = plot_confusion_matrix(modelo, X_valid_teste, y_valid_teste, display_labels=labels)
            fig.figure_.suptitle("Matriz de confusão")
            plt.show()
        except Exception as e:
            logging.error(e)
            fig2 = plot_confusion_matrix(modelo, X_valid_teste, y_valid_teste)
            fig2.figure_.suptitle("Matriz de confusão")
            plt.show()

    def processamento(self, colunas_para_excluir, classes_para_excluir, dataset):
        """
        metodo para processamento de limpeza do dataset

        :param colunas_para_excluir:
        :type colunas_para_excluir:
        :param classes_para_excluir:
        :type classes_para_excluir:
        :param dataset:
        :type dataset:
        :return:
        :rtype:
        """
        print('[!] Modfificando hora para coluna')
        dataset['OCORRENCIA_HORA'] = dataset['OCORRENCIA_HORA'].apply(self.tool.hora_para_turno)

        print(" /n ")
        print('[!] Retirando as classes filtradas do dataset...')
        for i in tqdm(range(len(classes_para_excluir))):
            dataset.drop(index=dataset[dataset['NATUREZA1_DESCRICAO'] == classes_para_excluir[i]].index, axis=0,
                         inplace=True)

        print('[!] Classes retiradas!')
        print('[!] Processamento dos valores faltando...')
        dataset.dropna(axis=0, subset=['NATUREZA1_DESCRICAO'], inplace=True)

        y_full = dataset['NATUREZA1_DESCRICAO']

        print('[!] Valores retirados!')

        print('[!] Processamento das colunas...')
        ordinalEncoder = preprocessing.OrdinalEncoder()
        y_full_preprocessado = ordinalEncoder.fit_transform(np.asarray(y_full).reshape(-1, 1))

        dataset.drop(columns=colunas_para_excluir + ['NATUREZA1_DESCRICAO'], axis=1, inplace=True)

        colunas_str = [cols for cols in dataset.columns if dataset[cols].dtype == 'object']
        colunas_num = [cols for cols in dataset.columns if dataset[cols].dtype in ['int64', 'float64']]

        for coluna in tqdm(colunas_str):
            try:
                dataset[coluna] = tool.transforma_coluna(dataset[coluna])
            except:
                dataset.drop(columns=coluna, axis=1, inplace=True)

        for coluna in tqdm(colunas_num):
            try:
                dataset[coluna] = tool.transforma_coluna_num(dataset[coluna])
            except:
                dataset.drop(columns=coluna, axis=1, inplace=True)

        print('[!] Processamento concluido!')

        return dataset, y_full_preprocessado

    def estimativa_de_previsao(self, dados, modelo):
        """
        description: esse metodo aqui serve para fazer uma entrada de usuario via tela para testar previsao,nao foi totalmente
        testado, entao nao considero parte do trabalho,
        """

        print(
            'A seguir, coloque os campos referente a cada coluna para a previsão do modelo. Digite !help para poder visualizar as opções disponíveis.')

        continuar = True

        while continuar:
            entrada = []
            for coluna in dados.columns:
                valor = input('Digite o dado referente para a coluna "%s": ' % (coluna))

                if coluna in ['OCORRENCIA_ANO', 'OCORRENCIA_MES']:
                    while not (valor in [str(int(i)) for i in dados[coluna].unique()]):
                        print('\n\n OPCOES DISPONIVEIS: \n')
                        print(dados[coluna].unique())
                        valor = input('Digite novamente o dado referente para a coluna "%s": ' % (coluna))

                    entrada.append(float(valor))

                else:
                    while not (valor in list(self.transformacao[coluna].categories_[0])):
                        print('\n\n OPCOES DISPONIVEIS: \n')
                        print(list(self.transformacao[coluna].categories_[0]))
                        valor = input('Digite novamente o dado referente para a coluna "%s": ' % (coluna))

                    entrada.append(self.transformacao[coluna].transform(np.array(valor).reshape(-1, 1))[0][0])

            resultado = self.transformacao['NATUREZA1_DESCRICAO'].inverse_transform(
                modelo.predict(self.standard_scaler.transform(np.asarray(entrada).reshape(1, 5))).reshape(-1,
                                                                                                          1))[0][
                0]
            print('\n\n\n RESULTADO DA PREDICAO: %s \n\n' % (resultado.upper()))

            if input('Realizar nova consulta? [s/n]') == 'n':
                continuar = False
