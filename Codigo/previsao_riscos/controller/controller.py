from tqdm import tqdm
import os
import logging
from utils import tools
from model import treino
from view import checagem_previsao

logging.getLogger().setLevel(logging.INFO)
logging.basicConfig(format='%(levelname)s - %(asctime)s %(message)s', datefmt='%Y-%m-%d %I:%M:%S %p')


class Controller:

    def __init__(self, dataset_treino, opcao, filename, dataset_teste):
        self.dataset = dataset_treino
        self.tool = tools.Tools()

        if int(opcao) > 0:
            logging.info("Efetuando pre-processamento - ETL")
            self.iniciar_pre_processamento(dataset_treino, filename)
        else:
            logging.info("Processando modelo ja limpo")
            self.dataset_processado, self.y_full, self.transformacao = self.processamento_dataset(dataset=self.dataset)

            self.dataset_processado_teste, self.y_full_teste, self.transformacao_teste = self.processamento_dataset(
                dataset=dataset_teste)

            treino.TreinoModelos(dataset_processado=self.dataset_processado,
                                 y_full=self.y_full,
                                 dado_original=dataset_treino,
                                 transformada=self.transformacao,
                                 dataset_processado_teste= self.dataset_processado_teste,
                                 y_full_teste=self.y_full_teste,
                                 dataset_original_teste =dataset_teste)

    def processamento_dataset(self, dataset):
        """
        Retirando as colunas faltando dados,fazendo o processamento das colunas e separa o target

        :param dataset:
        :type dataset:
        :return:
        :rtype:
        """
        # print(dataset['NATUREZA1_DESCRICAO'])
        transformacao = {}

        print('[!] Processamento dos dados...')

        colunas_str = [cols for cols in dataset.columns if dataset[cols].dtype == 'object']

        for coluna in tqdm(colunas_str):
            dataset[coluna], transformacao[coluna] = self.tool.transforma_coluna(dataset[coluna])

        y_full = dataset['NATUREZA1_DESCRICAO']
        dataset.drop(columns=['NATUREZA1_DESCRICAO'], axis=1, inplace=True)

        print('[!] Processamento concluido!')

        return dataset, y_full, transformacao

    def iniciar_pre_processamento(self, dados, filename):
        """
        Metodo para excluir colunas que nao serao usadas, e filtrar classes com maior numero de amostras
        :param dados:
        :type dados:
        """

        # colunas_excluir = ['NUMERO_PROTOCOLO_156', 'OCORRENCIA_CODIGO', 'Unnamed: 35',
        #                    'Unnamed: 36', 'Unnamed: 37', 'LOGRADOURO_NOME', 'SECRETARIA_NOME',
        #                    'SECRETARIA_SIGLA', 'EQUIPAMENTO_URBANO_NOME', 'OPERACAO_DESCRICAO',
        #                    'SITUACAO_EQUIPE_DESCRICAO', 'NUMERO_PROTOCOLO_156', 'SUBCATEGORIA2_DESCRICAO',
        #                    'SUBCATEGORIA3_DESCRICAO', 'SUBCATEGORIA4_DESCRICAO', 'SUBCATEGORIA5_DESCRICAO',
        #                    'NATUREZA5_DESCRICAO', 'NATUREZA5_DEFESA_CIVIL', 'NATUREZA4_DEFESA_CIVIL',
        #                    'NATUREZA4_DESCRICAO', 'NATUREZA3_DEFESA_CIVIL', 'NATUREZA3_DESCRICAO',
        #                    'NATUREZA2_DEFESA_CIVIL', 'NATUREZA2_DESCRICAO', 'OCORRENCIA_DATA',
        #                    'ATENDIMENTO_ANO', 'NATUREZA1_DEFESA_CIVIL', 'FLAG_EQUIPAMENTO_URBANO']

        # colunas_excluir = ['NUMERO_PROTOCOLO_156', 'OCORRENCIA_CODIGO', 'Unnamed: 35',
        #                    'Unnamed: 36', 'Unnamed: 37', 'LOGRADOURO_NOME', 'SECRETARIA_NOME',
        #                    'SECRETARIA_SIGLA', 'EQUIPAMENTO_URBANO_NOME', 'OPERACAO_DESCRICAO',
        #                    'SITUACAO_EQUIPE_DESCRICAO', 'NUMERO_PROTOCOLO_156', 'SUBCATEGORIA2_DESCRICAO',
        #                    'SUBCATEGORIA3_DESCRICAO', 'SUBCATEGORIA4_DESCRICAO', 'SUBCATEGORIA5_DESCRICAO',
        #                    'NATUREZA5_DESCRICAO', 'NATUREZA5_DEFESA_CIVIL', 'NATUREZA4_DEFESA_CIVIL',
        #                    'NATUREZA4_DESCRICAO', 'NATUREZA3_DEFESA_CIVIL', 'NATUREZA3_DESCRICAO',
        #                    'NATUREZA2_DEFESA_CIVIL', 'NATUREZA2_DESCRICAO', 'OCORRENCIA_DATA',
        #                    'ATENDIMENTO_ANO', 'NATUREZA1_DEFESA_CIVIL', 'FLAG_EQUIPAMENTO_URBANO']

        colunas_excluir = ['OCORRENCIA_CODIGO','NUMERO_PROTOCOLO_156','Unnamed: 35','Unnamed: 36',
                           'SECRETARIA_NOME', 'SECRETARIA_SIGLA','Unnamed: 37','Unnamed: 0',
                           'EQUIPAMENTO_URBANO_NOME', 'OPERACAO_DESCRICAO', 'LOGRADOURO_NOME',
                           'SITUACAO_EQUIPE_DESCRICAO', 'NUMERO_PROTOCOLO_156', 'SUBCATEGORIA2_DESCRICAO',
                           'SUBCATEGORIA3_DESCRICAO', 'SUBCATEGORIA4_DESCRICAO', 'SUBCATEGORIA5_DESCRICAO',
                           'NATUREZA5_DESCRICAO', 'NATUREZA5_DEFESA_CIVIL', 'NATUREZA4_DEFESA_CIVIL',
                           'NATUREZA4_DESCRICAO', 'NATUREZA3_DEFESA_CIVIL', 'NATUREZA3_DESCRICAO',
                           'NATUREZA2_DEFESA_CIVIL', 'NATUREZA2_DESCRICAO', 'OCORRENCIA_DATA',
                           'ATENDIMENTO_ANO', 'NATUREZA1_DEFESA_CIVIL', 'FLAG_EQUIPAMENTO_URBANO',
                           'FLAG_FLAGRANTE', 'SUBCATEGORIA1_DESCRICAO', 'ORIGEM_CHAMADO_DESCRICAO',
                           'REGIONAL_FATO_NOME', 'SERVICO_NOME']

        classes_para_excluir = self.tool.filtrar_classes(numero_mim_amostras=1000, dataset=dados)

        self.pre_processamento_dataset(colunas_excluir, classes_para_excluir, dados)

        # ### Salvando o processando em um arquivo csv

        if not os.path.exists('normalizado'):
            os.makedirs('normalizado')

        dados.to_csv('normalizado/' + filename + '_normalizado' + '.csv')

    def pre_processamento_dataset(self, colunas_para_excluir, classes_para_excluir, dataset):
        """
        metodo para pre processamento e limpeza inicial, faz conversao dos horarios para 4 turnos e excluir as classes
        to target com maior ocorrencia,remove ruidos e corrige alguns nomes com problema de encode

        :param colunas_para_excluir:
        :type colunas_para_excluir:
        :param classes_para_excluir:
        :type classes_para_excluir:
        :param dataset:
        :type dataset:
        """
        dataset['OCORRENCIA_HORA'] = dataset['OCORRENCIA_HORA'].apply(self.tool.hora_para_turno)

        for i in tqdm(range(len(classes_para_excluir))):
            dataset.drop(index=dataset[dataset['NATUREZA1_DESCRICAO'] == classes_para_excluir[i]].index, axis=0,
                         inplace=True)

        dataset.dropna(axis=0, subset=['NATUREZA1_DESCRICAO'], inplace=True)
        dataset.drop(columns=colunas_para_excluir, axis=1, inplace=True)

        dataset['NATUREZA1_DESCRICAO'] = dataset['NATUREZA1_DESCRICAO'].apply(
            self.tool.descricao_para_ocorrencias_macro)

        for coluna in tqdm(dataset.columns):
            print(coluna)
            dataset[coluna] = self.tool.retirando_nan_values(dataset[coluna])

        colunas_strings = [cols for cols in dataset.columns if dataset[cols].dtype == 'object']

        for col in tqdm(colunas_strings):
            dataset[col] = dataset[col].apply(self.tool.desbugar_nome)
