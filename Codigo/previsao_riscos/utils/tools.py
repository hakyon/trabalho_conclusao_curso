import logging

from sklearn import preprocessing
from sklearn.impute import SimpleImputer
import numpy as np


class Tools:
    # classe com metodos para tratamento e limpeza do dataset
    def __init__(self):

        # lista de nomes bugados
        self.nomes_bugados = {'Agress�o f�sica': 'Agressao fisica', 'Subst�ncia Il�cita': 'Substancia Ilicita',
                              'S�TIO': 'SITIO', 'C�NDIDA': 'CANDIDA', 'BOQUEIR�O': 'BOQUEIRAO', 'TABO�O': 'TABOAO',
                              'REBOU�AS': 'REBOUCAS',
                              '�GUA': 'AGUA', 'CAP�O': 'CAPAO', 'BOT�NICO': 'BOTANICO', 'Invas�o': 'Invasao',
                              'S�O CRISTOV�O': 'SAO CRISTOVVO',
                              'PORT�O': 'PORTAO', 'S�O LOUREN�O': 'SAO LOURENCO', 'S�O JO�O': 'SAO JOAO', 'S�O': 'SAO',
                              'C�VICO': 'CIVICO',
                              'UMBAR�': 'UMBARA', 'AM�RICAS': 'AMERICAS', 'LIND�IA': 'LINDOIA', 'JO�O': 'JOAO',
                              'TARUM�': 'TURUMA',
                              'MOSSUNGU�': 'MOSSUNGUE', 'IN�CIO': 'INACIO', 'GUA�RA': 'GUAIRA', 'AH�': 'AHU',
                              'GL�RIA': 'GLORIA',
                              'SEMIN�RIO': 'SEMINARIO', 'QUIT�RIA': 'QUINTERIA', 'JUVEV�': 'JUVEVE', 'MERC�S': 'MERCES',
                              'INDICA��ES': 'INDICACOES', 'FICT�CIO': 'FICTICIO', 'TING�I': 'TINGUI', 'N�O': 'NAO',
                              'CRISTOV�O': 'CRISTOVAO',
                              'MARACAN�': 'MARACANA', 'BANC�RIA': 'BANCARIA', 'IGUA�U': 'IGUACU', 'NA��ES': 'NACOES',
                              'IGUA��': 'IGUACU',
                              'RO�A': 'ROCA', 'Tr�nsito': 'Transito', 'Subst�ncia': 'Substancia', 'Il�cita': 'Ilicita',
                              'Perturba��o': 'Pertubacao', 'Amea�a': 'Ameaca', 'Agress�o': 'Agressao',
                              'f�sica': 'fisica',
                              'viola��o': 'violacao',
                              'C�o': 'Cao', 'p�blica': 'publica', 'patrim�nio': 'patrimonio', 'p�blico': 'publico',
                              'cidad�o': 'cidadao',
                              'PRESTA��O': 'PRESTACAO', 'Tr�fico': 'Trafico', 'ORIENTA��O': 'ORIENTACAO',
                              'el�trica': 'eletrica',
                              'Picha��o': 'Pichacao', 'Ve�culo': 'Veiculo',
                              'Apoio � outros �rg�os': 'Apoio a outros orgaos',
                              '�rg�os': 'orgaos',
                              'Omiss�o': 'Omissao', 'Apoio � SMS': 'Apoio a SMS', 'comunica��o': 'comunicacao',
                              'Apoio � PMPR': 'Apoio a PMPR',
                              'Apoio � URBS': 'Apoio a URBS', 'Ocupa��o': 'Ocupacao', 'Apoio � FAS': 'Apoio a FAS',
                              'resid�ncia': 'residencia',
                              'Apoio � SMAD': 'Apoio a SMAD', 'Apoio � SMU': 'Apoio a SMU',
                              'Apoio � Pol�cia Civil': 'Apoio a Policia Civil',
                              'Pol�cia': 'Policia', ' � ': ' a ', 'estima��o': 'estimacao', 'Dire��o': 'Direcao',
                              'Vigil�ncia': 'Vigilancia',
                              'Sanit�ria': 'Sanitaria', 'Rodovi�ria': 'Rodoviaria', 'Com�rcio': 'Comercio',
                              'Institui��o': 'Instituicao',
                              'S�BADO': 'SABADO', 'TER�A': 'TERCA', '� VIATURA': 'VIATURA', 'OF�CIO': 'OFICIO',
                              'REGI�O': 'REGIAO',
                              'OPERA��O': 'OPERACAO', 'Opera��es': 'Operacoes', 'C�es': 'Caes',
                              'COMUNIT�RIOS': 'COMUNITARIOS',
                              'FISCALIZA��O': 'FISCALIZACAO', 'PROTE��O': 'PROTECAO', 'M�NICA': 'MONICA',
                              'Equ�deo': 'Equideo',
                              'muni��o': 'municao', 'recupera��o': 'recuperacao', 'domic�lio': 'domicilio',
                              'desintelig�ncia': 'desinteligencia',
                              'c�u': 'ceu', 'situa��o': 'situacao', 'satura��o': 'situacao'}

    def transforma_coluna(self, coluna):
        """

        :param coluna:
        :type coluna:
        :return:
        :rtype:
        """
        oe = preprocessing.OrdinalEncoder()
        oe.fit(np.array(coluna).reshape(-1, 1))

        return oe.transform(np.array(coluna).reshape(-1, 1)), oe

    def transforma_coluna_num(self, coluna):
        """
        convertendo colunas de descricao para numericas
        :param coluna:
        :type coluna:
        :return:
        :rtype:
        """
        si = SimpleImputer(strategy='most_frequent')

        return si.fit_transform(np.asarray(coluna).reshape(-1, 1))

    def hora_para_turno(self, x):
        """
        Função para modificar os horários para turno (manhã, tarde, noite, madrugada)
        :rtype: object
        """

        try:
            hora = int(x.split(':')[0])
        except Exception as e:
            return 'Madrugada'

        if hora < 6:
            return 'Madrugada'
        elif hora < 12:
            return 'Manha'
        elif hora < 18:
            return 'Tarde'
        else:
            return 'Noite'


    def defini_crime(self, ocorrencia):
        """
        Função para modificar definir de maneira binaria a existencia de um crime ou nao
        :rtype: object
        """


        if hora < 6:
            return 'Madrugada'
        elif hora < 12:
            return 'Manha'
        elif hora < 18:
            return 'Tarde'
        else:
            return 'Noite'


    def descricao_para_ocorrencias_macro(self, x):
        """
        Função para modificar os horários para turno (manhã, tarde, noite, madrugada)
        :rtype: object
        """

        if x == 'Roubo' or x == 'Invas�o' or x == 'Invasao':
            return 'policial grave'
        elif x == 'Perturba��o do sossego' or x == 'Subst�ncia Il�cita' or x == 'Alarmes' or x == 'Perturbacao do sossego' or x == 'Substancia Ilicita':
            return 'policial leve'
        elif x == 'Dano' or x == 'Animais':
            return 'ambiental'
        elif x == 'Apoio' or x == 'Tr�nsito' or x == 'Abordagem' or x == 'Atitude suspeita':
            return 'apoio'
        else:
            return 'nao categorizada'

    def filtrar_classes(self, numero_mim_amostras, dataset):
        """
         Função para filtrar as classes para que restem apenas as com maiores números de amostras.

        :param numero_mim_amostras:
        :type numero_mim_amostras:
        :param dataset:
        :type dataset:
        :return:
        :rtype:
        """

        classe = {}

        for i in dataset['NATUREZA1_DESCRICAO'].unique():
            classe[i] = sum(dataset['NATUREZA1_DESCRICAO'] == i)

        classes_resultantes = []
        classes_jubilar = []
        for i in classe.keys():
            if classe[i] < numero_mim_amostras:
                classes_jubilar.append(i)
            else:
                classes_resultantes.append(i)

        print('[!] Classes de ocorrencia com maior representacao :')
        print(classes_resultantes)
        return classes_jubilar

    def desbugar_nome(self, x):
        """
        #meotod para limpeza de caracteres bugados
        :param x:
        :type x:
        :return:
        :rtype:
        """
        #palavras = x.split(' ')
        try:
            palavras = x.split(' ')
        except Exception as e:
            palavras = ''

        for idx, palavra in enumerate(palavras):
            for nome in self.nomes_bugados.keys():
                if nome.lower() in palavra.lower():
                    palavras[idx] = self.nomes_bugados[nome]

                elif len(palavra) < 2 and palavra == '�':
                    palavras[idx] = 'a'

        return ''.join(str(e).lower() + ' ' for e in palavras)[:-1]

    def retirando_nan_values(self, coluna):
        """
        metodo para retirar as colunas faltando dados
        :return:
        :rtype:
        """

        si = SimpleImputer(strategy='most_frequent')
        try:
            return si.fit_transform(np.asarray(coluna).reshape(-1, 1))
        except Exception as e:
            print(coluna)
        #return si.fit_transform(np.asarray(coluna).reshape(-1, 1))
