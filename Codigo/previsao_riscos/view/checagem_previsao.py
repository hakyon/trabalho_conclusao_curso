import numpy as np
import xgboost
from xgboost import XGBRegressor
from sklearn import preprocessing
from sklearn.naive_bayes import GaussianNB

xgboost = XGBRegressor(n_estimators=20000, learning_rate=0.05)


class View:

    def __init__(self, dados, transformacao):
        self.dados = dados
        self.transformacao = transformacao

    def estimativa_svn_regressao(self):

        standard_scaler = preprocessing.StandardScaler()

        print(
            'A seguir, coloque os campos referente a cada coluna para a previsão do modelo. Digite !help para poder visualizar as opções disponíveis.')

        continuar = True

        while continuar:
            entrada = []
            for coluna in self.dados.columns:
                valor = input('Digite o dado referente para a coluna "%s": ' % (coluna))

                if coluna in ['OCORRENCIA_ANO', 'OCORRENCIA_MES']:
                    while not (valor in [str(int(i)) for i in self.dados[coluna].unique()]):
                        print('\n\n OPCOES DISPONIVEIS: \n')
                        print(self.dados[coluna].unique())
                        valor = input('Digite novamente o dado referente para a coluna "%s": ' % (coluna))

                    entrada.append(float(valor))

                else:
                    while not (valor in list(self.transformacao[coluna].categories_[0])):
                        print('\n\n OPCOES DISPONIVEIS: \n')
                        print(list(self.transformacao[coluna].categories_[0]))
                        valor = input('Digite novamente o dado referente para a coluna "%s": ' % (coluna))

                    entrada.append(self.transformacao[coluna].transform(np.array(valor).reshape(-1, 1))[0][0])

            resultado = self.transformacao['NATUREZA1_DESCRICAO'].inverse_transform(
                xgboost.predict(standard_scaler.transform(np.asarray(entrada).reshape(1, 11))).reshape(-1, 1))[0][
                0]
            print('\n\n\n RESULTADO DA PREDICAO: %s \n\n' % (resultado.upper()))

            if input('Realizar nova consulta? [s/n]') == 'n':
                continuar = False

    def estimativa_naive_bayes_gausiano(self):

        standard_scaler = preprocessing.StandardScaler()

        print(
            'A seguir, coloque os campos referente a cada coluna para a previsão do modelo. Digite !help para poder visualizar as opções disponíveis.')

        continuar = True

        while continuar:
            entrada = []
            for coluna in self.dados.columns:
                valor = input('Digite o dado referente para a coluna "%s": ' % (coluna))

                if coluna in ['OCORRENCIA_ANO', 'OCORRENCIA_MES']:
                    while not (valor in [str(int(i)) for i in self.dados[coluna].unique()]):
                        print('\n\n OPCOES DISPONIVEIS: \n')
                        print(self.dados[coluna].unique())
                        valor = input('Digite novamente o dado referente para a coluna "%s": ' % (coluna))

                    entrada.append(float(valor))

                else:
                    while not (valor in list(self.transformacao[coluna].categories_[0])):
                        print('\n\n OPCOES DISPONIVEIS: \n')
                        print(list(self.transformacao[coluna].categories_[0]))
                        valor = input('Digite novamente o dado referente para a coluna "%s": ' % (coluna))

                    entrada.append(self.transformacao[coluna].transform(np.array(valor).reshape(-1, 1))[0][0])

            resultado = self.transformacao['NATUREZA1_DESCRICAO'].inverse_transform(
                GaussianNB.predict(standard_scaler.transform(np.asarray(entrada).reshape(1, 11))).reshape(-1, 1))[0][0]
            print('\n\n\n RESULTADO DA PREDICAO: %s \n\n' % (resultado.upper()))

            if input('Realizar nova consulta? [s/n]') == 'n':
                continuar = False
