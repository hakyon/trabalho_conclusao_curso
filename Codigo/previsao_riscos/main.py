import gzip

import pandas as pd
import os
from controller import controller
import logging

## O procedimento inidial do codigo consiste apenas em duas vias
## A oopcao 1 executa a limpeza do dataset e salva em uma pasta chamada normalizado
## Opcao 2 ja executa os testes com modelo

logging.getLogger().setLevel(logging.INFO)
logging.basicConfig(format='%(levelname)s - %(asctime)s %(message)s', datefmt='%Y-%m-%d %I:%M:%S %p')


def main():
    #file_path = 'datasets/base_dados_processado_completo.csv'

    # nesse trecho, ao precisar efetuar o pre-processamento, deve ser substiuido aqui com o path do arquivo analisado
    # ao final do processo ele vai salvar na raiz dentro de uma pasta nomeada normalizacao
    # depois de efetuado normalizacao, entao basta apontar no path esse mesmo caminho do arquivo limpo

    #file_path = 'datasets/experimento01/base2017_normalizado.csv'
    #file_path = 'datasets/experimento02/base2018_normalizado.csv'
    #file_path = 'datasets/experimento03/base2016-2017_normalizado.csv'
    #file_path = 'datasets/experimento04/base2017-2018_normalizado.csv'
    #file_path = 'datasets/experimento05/base2015-2018_normalizado.csv'
    #file_path = 'datasets/experimento06/base2016-2018_normalizado.csv'
    file_path = 'datasets/experimento07/base2015-2018_normalizado.csv'


    #file_path = 'datasets/base2016-2018.csv'

    #file_path = 'normalizado/base2012-2013_treino_normalizado.csv'
    file_name = os.path.basename(file_path)
    file_name = file_name[:file_name.find(".")]

    #dados = pd.read_csv(file_path, encoding='ISO-8859–1')

    dados = pd.read_csv(file_path, index_col='Unnamed: 0')

    teste = pd.read_csv('datasets/testes/normalizado/base2019_normalizado.csv', index_col='Unnamed: 0')

    opcao = input('Fazer pre-processamento ? (1-Sim/0-Nao): ')

    if int(opcao) == 0:
        if not os.path.exists('normalizado'):
            print('Nao ha dataset normalizado, execute pre-processamento antes')
            return

    controller.Controller(dataset_treino=dados, opcao=opcao, filename=file_name,dataset_teste=teste)


if __name__ == '__main__':
    main()
