

### TCC : Analise de locais de riscos baseado em datasets da Guarda Municipal de Curitiba ###

* Periodo : 11/08/2020
* Version 1.00
* Ambiente : Desktop

### Ferramentas usadas ###

* Linguagem: Python 3.8
* Framework : 
* Plataforma : Linux/Windows
* IDE : Pycharm 2020

Depois de processado os dados usando tecnicas de aprendizado de maquina sera possivel

apartir de dados de entrada,saber saber qual sao as probabilidades de determinados incidentes ocorrerem novamente.
E os dados de entrada sao:

Bairro
Hora
Dia-semana

Iunstrucao de uso:

Na primesira execucao do algoritmo, ele vai perguntar se deseja efetuar um pre-processamento, que venha a ser uma limpeza do 
dataset, 

portanto o arquivo escolhido sera aquele estiver setado na variavel path , conforme aquivo da main

Exemplo

    file_path = 'datasets/base2015-2018_treino.csv'

Depois  se seleciona essa opcao ele ira fazer a limpeza e salvar o mesmo arquivo ja limpo com a palavra normalizado junto 

Exemplo:

    file_path = 'normalizado/base2015-2018_treino_normalizado.csv'

O programa ira terminar a execucao. Na segunda execucao, seleciona 0 para escolher nao efetuar pre-processamento

Depois disso sera solicitado para usuario escolher qual tecnica sera ultilizado , e com isso sera executado e exibido o
relatorio de resultado  

### Autor  ###

* Alan Azevedo Bancks

### Contato ###

* E-mail: dsalan@hotmail.com

### Observacões ###

